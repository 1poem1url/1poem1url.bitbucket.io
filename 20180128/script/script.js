$(function() {


//////////font-feature-settings切り替え
  $('#check1').change(function(){

    if($(this).is(':checked')){
      $('body').addClass('font-feature');
    }
    else{
      $('body').removeClass('font-feature');
    }

  });

//////////overflow-wrap切り替え
  $('#check2').change(function(){

    if($(this).is(':checked')){
      $('body').addClass('overflow-wrap');
    }
    else{
      $('body').removeClass('overflow-wrap');
    }

  });

//////////line-heightを変更
  $('#pull1').change(function(){

    var val1 = ($(this).val()) + "em";
    $('p').css('line-height',val1);

  });

//////////letter-spacingを変更
  $('#pull2').change(function(){

    var val2 = ($(this).val()) + "em";
    $('p').css('letter-spacing',val2);

  });


//////////control-areaを固定
  var nav =$('#globalNavi'),
  offset = nav.offset();

  $(window).scroll(function () {
	  if($(window).scrollTop() > offset.top) {
	    nav.addClass('fixed');
	  } else {
	    nav.removeClass('fixed');
	  }
	});

});
